import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Key, KeyChord, Screen, Group, Drag, Click, Match
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from typing import List  # noqa: F401
from os import path
import colors
import distro
from qtile_extras import widget
from qtile_extras.widget.decorations import RectDecoration
from fonts import groupbox_font, default_font, powerline_font, icon_font, tray_font, temp_icon_font, cpu_icon_font, os_font, wm_font
from qtile_extras import widget
from qtile_extras.widget.decorations import RectDecoration

colors = colors.colors

def init_layout_theme():
        return {
                "border_focus": colors[11],
                "border_normal": colors[18],
                "border_width": 4,
                }
def margin():
        return {
                'margin': 40,
                }

spacer_length = 10

clock_col = 9
kernel_col = 7
temp_col = 11
mem_col = 16
gpu_col = 13
cpu_col = 9
hdd_col = 7
vol_col = 11

def left_rect(bg=0):
    return [
            widget.Spacer(
                background=colors[0],
                length=10,
                decorations=[
                    RectDecoration(
                        colour=colors[bg],
                        radius = [10,0,0,10],
                        filled = True,
                        padding_y=0
                        )
                    ]
                )
            ]

def center_rect(bg=0):
    return {
            'decorations':[
                RectDecoration(
                    colour=colors[bg],
                    radius = 0,
                    filled = True,
                    padding_y=0
                    )
                ]
            }

def right_rect(bg=0):
    return [
            widget.Spacer(
                background=colors[0],
                length=10,
                decorations=[
                    RectDecoration(
                        colour=colors[bg],
                        radius = [0,10,10,0],
                        filled = True,
                        padding_y=0
                        )
                    ]
                )
            ]

def separator_widget():
    return [
            widget.Spacer(
                length = 10,
                background = colors[0]
                )
            ]


def os_widget():
    return [
            widget.Spacer(length = spacer_length, background = colors[0]),
            *left_rect(bg=18),
            widget.GenPollText(
                update_interval=1,
                func=lambda: subprocess.check_output("/home/greg/.local/bin/distro.sh").decode("utf-8"), 
                background = colors[0],
                foreground = colors[1],
                **os_font(),
                **center_rect(bg=18)
                ),
            *right_rect(bg=18),
            widget.Spacer(length = spacer_length, background = colors[0])
            ]

def groupbox_widget():
    return [
            *left_rect(bg=18),
            widget.GroupBox(
                hide_unused = False,
                margin_y = 2,
                margin_x = 0,
                padding_y = 0,
                padding_x = 5,
                borderwidth = 4,
                highlight_method = "line",                                   
                this_current_screen_border = colors[11],                     
                this_screen_border = colors[11],                             
                other_current_screen_border = colors[18],                    
                other_screen_border = colors[18],                            
                block_highlight_text_color = colors[7],                     
                active = colors[9],
                inactive = colors[19],
                rounded = True,
                highlight_color = colors[18],
                background = colors[18],
                foreground = colors[1],
                center_aligned = True,
                **groupbox_font(),
                **center_rect(bg=18)
                ),
            *right_rect(bg=18),
            widget.Spacer(length = spacer_length, background = colors[0])
            ]

def current_window():
    return [
            *left_rect(bg=18),
            widget.WindowName(
                background = colors[0],
                foreground = colors[7], 
                max_chars = 20, 
                padding = 10,
                width = 400,
                empty_group_string = 'Empty',
                **default_font(),
                **center_rect(18)
                ),
            *right_rect(bg=18),
            widget.Spacer(background = colors[0], length = bar.STRETCH),
            ]

def layout_widget():
    return [
            *left_rect(bg=18),
            widget.CurrentLayoutIcon(
                custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                background = colors[0],
                padding = 0, 
                scale = 0.5,
                **center_rect(bg=18)
                ),
            *right_rect(bg=18),
            widget.Spacer(background=colors[0], length=spacer_length)
        ]

def clock_widget():
    return [
            *left_rect(bg=2),
            widget.TextBox(
                text = '', 
                background = colors[0],
                foreground = colors[clock_col],
                **icon_font(),
                **center_rect(bg=2)
                ),
            widget.Spacer(
                background=colors[0], 
                length=spacer_length,
                **center_rect(bg=2)
                ),
            widget.Clock(
                background = colors[0],
                foreground = colors[1],
                format='%b-%d-%Y',
                **default_font(),
                **center_rect(bg=18)
                ),
            widget.GenPollText(
                update_interval=1,
                func=lambda: subprocess.check_output("/home/greg/.local/bin/time.sh").decode("utf-8"), 
                background = colors[0],
                foreground = colors[1],
                **default_font(),
                **center_rect(bg=18)
                ),
            *right_rect(bg=18),
            widget.Spacer(length=spacer_length, background = colors[0],)
        ]

def kernel_widget():
    return [
            *left_rect(bg=2),
            widget.TextBox(
                text = '', 
                background = colors[0],
                foreground = colors[7],
                **icon_font(),
                **center_rect(bg=2)
                ),
            widget.Spacer(
                background=colors[0], 
                length=spacer_length,
                **center_rect(bg=2)
                ),
            widget.GenPollText(
                update_interval=1,
                func=lambda: subprocess.check_output("/home/greg/.local/bin/kernel").decode("utf-8"), 
                background = colors[0],
                foreground = colors[1],
                **default_font(),
                **center_rect(bg=18)
                ),
            *right_rect(bg=18),
            widget.Spacer(length=spacer_length, background = colors[0],)
        ]

def temp_widget():
    return [
            *left_rect(bg=2),
            widget.TextBox(
                text = '', 
                background = colors[0],
                foreground = colors[11],
                **temp_icon_font(),
                **center_rect(bg=2)
                ),
            widget.Spacer(
                background=colors[0], 
                length=spacer_length,
                **center_rect(bg=2)
                ),
            widget.ThermalSensor(
                background = colors[0],
                foreground = colors[1],
                **default_font(),
                **center_rect(bg=18)
                ),
            *right_rect(bg=18),
            widget.Spacer(length=spacer_length, background = colors[0],)
        ]

def mem_widget():
    return [
            *left_rect(bg=2),
            widget.TextBox(
                text = '', 
                background = colors[0],
                foreground = colors[16],
                **icon_font(),
                **center_rect(bg=2)
                ),
            widget.Spacer(
                background=colors[0], 
                length=spacer_length,
                **center_rect(bg=2)
                ),
            widget.Memory(
                measure_mem= 'G', 
                background = colors[0],
                foreground = colors[1],
                **default_font(),
                **center_rect(bg=18)
                ),
            *right_rect(bg=18),
            widget.Spacer(length=spacer_length, background = colors[0],)
        ]

def gpu_widget(bg="bg",fg="fg"):
    return [
            *left_rect(bg=2),
            widget.TextBox(
                text = '󰡀', 
                background = colors[0],
                foreground = colors[13],
                **icon_font(),
                **center_rect(bg=2)
                ),
            widget.Spacer(
                background=colors[0], 
                length=spacer_length,
                **center_rect(bg=2)
                ),
            widget.NvidiaSensors(
                format = '{temp}°C', 
                background = colors[0],
                foreground = colors[1],
                **default_font(),
                **center_rect(bg=18)
                ),
            *right_rect(bg=18),
            widget.Spacer(length=spacer_length, background = colors[0],)
        ]

def cpu_widget(bg="bg",fg="fg"):
    return [
            *left_rect(bg=2),
            widget.TextBox(
                text = '', 
                background = colors[0],
                foreground = colors[9],
                **icon_font(),
                **center_rect(bg=2)
                ),
            widget.Spacer(
                background=colors[0], 
                length=spacer_length,
                **center_rect(bg=2)
                ),
            widget.CPU(
                format = '{freq_current}GHz {load_percent:>4.1f}%', 
                background = colors[0],
                foreground = colors[1],
                **default_font(),
                **center_rect(bg=18)
                ),
            *right_rect(bg=18),
            widget.Spacer(length=spacer_length, background = colors[0],)
        ]

def hdd_widget(bg="bg",fg="fg"):
    return [
            *left_rect(bg=2),
            widget.TextBox(
                text = '', 
                background = colors[0],
                foreground = colors[7],
                **icon_font(),
                **center_rect(bg=2)
                ),
            widget.Spacer(
                background=colors[0], 
                length=spacer_length,
                **center_rect(bg=2)
                ),
            widget.GenPollText(
                update_interval=1,
                func=lambda: subprocess.check_output("/home/greg/.local/bin/hdd").decode("utf-8"), 
                background = colors[0],
                foreground = colors[1],
                **default_font(),
                **center_rect(bg=18)
                ),
            *right_rect(bg=18),
            widget.Spacer(length=spacer_length, background = colors[0],)
        ]

def volume_widget(bg="bg", fg="fg"):
    return [
            *left_rect(bg=2),
            widget.TextBox(
                text = '󰕾', 
                background = colors[0],
                foreground = colors[11],
                **icon_font(),
                **center_rect(bg=2)
                ),
            widget.Spacer(
                background=colors[0], 
                length=spacer_length,
                **center_rect(bg=2)
                ),
            widget.GenPollText(
                update_interval=1,
                func=lambda: subprocess.check_output("/home/greg/.local/bin/pavolume.sh").decode("utf-8"),
                background = colors[0],
                foreground = colors[1],
                **default_font(),
                **center_rect(bg=18)
                ),
            *right_rect(bg=18),
            widget.Spacer(length=spacer_length, background = colors[0],)
        ]
              
def obs_widget(bg="bg", fg="fg"):
    return [
            widget.GenPollText(
                update_interval=1,
                func=lambda: subprocess.check_output("/home/greg/.local/bin/obs.sh").decode("utf-8"),
                background = colors[0],
                foreground = colors[1],
                **tray_font()
                )
        ]

def init_widgets_list():
    widgets_list = [
                *os_widget(),
                *groupbox_widget(), 
                *current_window(),    
                *volume_widget(),
                *hdd_widget(),
                *cpu_widget(),
                *gpu_widget(),
                *mem_widget(),
                *temp_widget(),
                *kernel_widget(),
                *clock_widget(),
                *layout_widget(),
    ]            
    return widgets_list

def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1                       # Slicing removes unwanted widgets on Monitors 1,3
def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    #del widgets_screen2[9]
    return widgets_screen2                       # Monitor 2 will display all widgets in widgets_list
def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), background="#00000000", border_width = [10,0,10,0], border_color="#1E1E2E", margin = [20,40,0,40], opacity=1.0, size=40)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), background="#00000000", border_width = [10,0,10,0], border_color="#1E1E2E", margin = [20,40,0,40], opacity=1.0, size=40))]

colors = [
    #Main Background
    ["#1E1E2E"], #0,  bg
    ["#CDD6F4"], #1,  fg
    ["#1b1b29"], #2,  black
    ["#F38BA8"], #3,  red
    ["#A6E3A1"], #4,  green
    ["#F9E2AF"], #5,  yellow
    ["#89B4FA"], #6,  blue
    ["#CBA6F7"], #7,  muave
    ["#94E2D5"], #8,  teal
    ["#f5c2e7"], #9,  pink
    ["#fab387"], #10, peach
    ["#eba0ac"], #11, maroon
    ["#f5e0dc"], #12, rosewater
    ["#f2cdcd"], #13, flamingo
    ["#89dceb"], #14, sky
    ["#74c7ec"], #15, sapphire
    ["#b4befe"], #16, lavender
    ["#A6ADC8"], #17, white
    ["#2a2a47"], #18, bright black
    ["#585d80"], #19, bright black 2
    ["#737899"]  #20, bright black 2
]

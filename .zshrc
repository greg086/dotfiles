# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
# Dependancies You Need for this Config
# zsh-syntax-highlighting - syntax highlighting for ZSH in standard repos
# autojump - jump to directories with j or jc for child or jo to open in file manager
# zsh-autosuggestions - Suggestions based on your history
# Initial Setup
# touch "$HOME/.cache/zshhistory
# Setup Alias in $HOME/zsh/aliasrc
# git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k
# echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >>! ~/.zshrc
# Enable colors and change prompt:
autoload -U colors && colors
setopt PROMPT_SUBST
setopt inc_append_history_time
#red
#PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "
PS1=$'%{\e[38;5;3;48;5;258m%}░▒▓%{\e[38;5;16;48;5;3m%}  %{\e[38;5;3;48;5;5m%}%{\e[38;5;16;48;5;5m%}  %~ %{\e[38;5;5;48;5;4m%}%{\e[38;5;16;48;5;4m%} %n@%M %{\e[0m%}%{\e[38;5;4m%}%{\e[0m%} '
#PS1="%B%{$fg[green]%}[%{$fg[yellow]%}%n%{$fg[blue]%}@%{$fg[yellow]%}%M %{$fg[blue]%}%~%{$fg[green]%}]%{$reset_color%}$%b "
set_command_check () {
  if [[ ${(%):-%?} = 0 ]]; then
    CHECK="%F{yellow} ✔ %f"
  else
    CHECK="%F{red} ✘ %f"
  fi
}

typeset -a precmd_functions
precmd_functions+=(set_command_check)

typeset -a precmd_functions
precmd_functions+=(set_red_prompt_background)
#RPROMPT=$'%{\e[38;5;0m%}%{\e[48;5;0m%}$CHECK%{\e[38;5;8;48;5;0m%}%{\e[38;5;15;48;5;8m%}  $(kernel) %{\e[38;5;15;48;5;8m%}%{\e[38;5;16;48;5;15m%}  %t %{\e[0m%}%{\e[38;5;15m%}▓▒░%{\e[0m%}'
# Custom Variables
export TERM="st-256color"
export EDITOR="nvim"
export VISUAL="nvim"
### Archive Extraction
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;  
      *.tar.gz)    tar xzf $1   ;;  
      *.bz2)       bunzip2 $1   ;;  
      *.rar)       unrar x $1   ;;  
      *.gz)        gunzip $1    ;;  
      *.tar)       tar xf $1    ;;  
      *.tbz2)      tar xjf $1   ;;  
      *.tgz)       tar xzf $1   ;;  
      *.zip)       unzip $1     ;;  
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;  
      *.deb)       ar x $1      ;;  
      *.tar.xz)    tar xf $1    ;;  
      *.tar.zst)   unzstd $1    ;;    
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi  
}
### Aliases
# Interactive File Management
alias mv='mv -i'    
alias cp='cp -i'
alias rm='rm -i'
### Navigation
# cd ..
alias ..='cd ..' 
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'
### pacman
alias ul='sudo rm /var/lib/pacman/db.lck'
### Git
alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME' 
alias gc='git clone'
alias gi='git init'
alias gr='git remote add origin'
alias ga='git add .'
alias gcm='git commit -m'
alias gp='git push -u origin master'
alias gf='git push -f origin master'
alias grn='git remote rename origin old-origin'
alias gra='git remote add origin'
alias gpa='git push -u origin --all'
alias gpt='git push -u origin --tags'
alias gpaf="git push -f origin --all"
alias gptf='git push -f origin --tags'
### Changing ls to exa
alias ls='exa -la --color=always --group-directories-first'
alias la='exa -a --color=always --group-directories-first'
alias ll='exa -l --color=always --group-directories-first'
alias lt='exa -aT --color=always --group-directories-first'
### Clear and Neofetch
alias clr='clear'
alias neofetch='neofetch --off'
alias neo='neofetch --off'
alias clrn='clear && neofetch --off'
### Other
alias df='df -h'
alias j='jobs'
alias pu='pushd'
alias po='popd'
alias d='dirs -v'
alias h='history'
alias grep='egrep'
### Vim
alias vim="nvim"
# History in cache directory:
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zshhistory
setopt appendhistory
# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)               # Include hidden files.
# Custom ZSH Binds
bindkey '^ ' autosuggest-accept
# Load aliases and shortcuts if existent.
# [ -f "$HOME/zsh/aliasrc" ] && source "$HOME/zsh/aliasrc"
# Load ; should be last.
#Arch
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
source /usr/share/autojump/autojump.zsh 2>/dev/null
#Debian
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
source /usr/share/autojump/autojump.zsh 2>/dev/null
#Debian
source /usr/share/zsh/site-contrib/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
source /usr/share/zsh/site-functions/zsh-syntax-highlighting.zsh 2>/dev/null
source /usr/share/zsh/site-functions/autojump/autojump.zsh 2>/dev/null

eval "$(starship init zsh)"
#neofetch

# Created by `userpath` on 2021-01-04 21:36:43
export PATH="$PATH:/home/greg/.local/bin"


PATH=$PATH:/home/greg/010editor;export PATH; # ADDED BY INSTALLER - DO NOT EDIT OR DELETE THIS COMMENT - 87FF8EFC-483D-BCAA-D67D-735CF60410D1 905C1B12-23B1-FCCC-B711-75BD7DBBD133

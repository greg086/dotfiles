#
# ~/.bashrc
#
[[ $- != *i* ]] && return
colors() {
	local fgc bgc vals seq0
	printf "Color escapes are %s\n" '\e[${value};...;${value}m'
	printf "Values 30..37 are \e[33mforeground colors\e[m\n"
	printf "Values 40..47 are \e[43mbackground colors\e[m\n"
	printf "Value  1 gives a  \e[1mbold-faced look\e[m\n\n"
	# foreground colors
	for fgc in {30..37}; do
		# background colors
		for bgc in {40..47}; do
			fgc=${fgc#37} # white
			bgc=${bgc#40} # black
			vals="${fgc:+$fgc;}${bgc}"
			vals=${vals%%;}
			seq0="${vals:+\e[${vals}m}"
			printf "  %-9s" "${seq0:-(default)}"
			printf " ${seq0}TEXT\e[m"
			printf " \e[${vals:+${vals+$vals;}}1mBOLD\e[m"
		done
		echo; echo
	done
}
export TERM="st-256color"
export EDIDOR="nvim"
export VISUAL="nvim"
#export PAGER="/bin/sh -c \"unset PAGER;col -b -x | \
    #nvim -R -c 'set ft=man nomod nolist' -c 'map q :q<CR>' \
    #-c 'map <SPACE> <C-D>' -c 'map b <C-U>' \
    #-c 'nmap K :Man <C-R>=expand(\\\"<cword>\\\")<CR><CR>' -\""
export PAGER="less"


#Catppuccin exa
#export EXA_COLORS="\
#di=95;01:\
#uu=94:\
#gu=31:\
#sn=93;01:\
#sb=93;01:\
#df=93;01:\
#ds=93;01:\
#da=95:\
#ln=34:\
#lp=34:\
#or:93:\
#ur=94;01:\
#uw=93;01:\
#ux=95;01:\
#ue=95;01:\
#gr=94:\
#gw=93:\
#gx=95:\
#tr=94:\
#tw=93:\
#tx=95:"

#Dracula exa
#export EXA_COLORS="\
#uu=36:\
#gu=37:\
#sn=32:\
#sb=32:\
#df=93;\
#ds=93;\
#da=34:\
#ur=34:\
#uw=35:\
#ux=36:\
#ue=36:\
#gr=34:\
#gw=35:\
#gx=36:\
#tr=34:\
#tw=35:\
#tx=36:"

#export MANPAGER="nvim -c 'set ft=man' -"
[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion
# Change the window title of X terminals
case ${TERM} in
	xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|interix|konsole*)
		PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
		;;
	screen*)
		PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
		;;
esac
use_color=true
# Set colorful PS1 only on colorful terminals.
# dircolors --print-database uses its own built-in database
# instead of using /etc/DIR_COLORS.  Try to use the external file
# first to take advantage of user additions.  Use internal bash
# globbing instead of external grep binary.
safe_term=${TERM//[^[:alnum:]]/?}   # sanitize TERM
match_lhs=""
[[ -f ~/.dir_colors   ]] && match_lhs="${match_lhs}$(<~/.dir_colors)"
[[ -f /etc/DIR_COLORS ]] && match_lhs="${match_lhs}$(</etc/DIR_COLORS)"
[[ -z ${match_lhs}    ]] \
	&& type -P dircolors >/dev/null \
	&& match_lhs=$(dircolors --print-database)
[[ $'\n'${match_lhs} == *$'\n'"TERM "${safe_term}* ]] && use_color=true
if ${use_color} ; then
	# Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
	if type -P dircolors >/dev/null ; then
		if [[ -f ~/.dir_colors ]] ; then
			eval $(dircolors -b ~/.dir_colors)
		elif [[ -f /etc/DIR_COLORS ]] ; then
			eval $(dircolors -b /etc/DIR_COLORS)
		fi
	fi
	if [[ ${EUID} == 0 ]] ; then
		PS1='\[\033[01;31m\][\h\[\033[01;36m\] \W\[\033[01;31m\]]\$\[\033[00m\] '
	else
		PS1='\[\033[01;31m\][\033[01;33m\]\u\[\033[01;32m\]@\033[01;34m\]\h \033[01;35m\]\W\[\033[01;31m\]]\033[01;37m\]\$\[\033[00m\] '
		#PS1='\[\e[38;5;3;48;5;258m\]░▒▓\e[38;5;16;48;5;3m\]  \e[38;5;3;48;5;5m\]\e[38;5;16;48;5;5m\]  \W \e[38;5;5;48;5;4m\]\e[38;5;16;48;5;4m\] \u@\h \[\033[00m\]\e[38;5;4m\]\[\033[00m\] '
	fi
	alias ls='ls --color=auto'
	alias grep='grep --colour=auto'
	alias egrep='egrep --colour=auto'
	alias fgrep='fgrep --colour=auto'
else
	if [[ ${EUID} == 0 ]] ; then
		# show root@ when we don't have colors
		PS1='\u@\h \W \$ '
	else
		PS1='\u@\h \w \$ '
	fi
fi
unset use_color safe_term match_lhs sh
xhost +local:root > /dev/null 2>&1
complete -cf sudo
# Bash won't get SIGWINCH if another process is in the foreground.
# Enable checkwinsize so that bash will check the terminal size when
# it regains control.  #65623
# http://cnswww.cns.cwru.edu/~chet/bash/FAQ (E11)
shopt -s checkwinsize
shopt -s expand_aliases
# export QT_SELECT=4
# Enable history appending instead of overwriting.  #139609
shopt -s histappend
#
# # ex - archive extractor
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}
### Aliases
# Interactive File Management
alias mv='mv -i'
alias cp='cp -i'
alias rm='rm -i'
#Vim
#alias vim='nvim'
### Navigation
# cd
alias rt='cd /'
alias de='cd ~/Desktop'
alias doc='cd ~/Documents'
alias dw='cd ~/Downloads'
# cd ..
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'
### Git
alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
alias gc='git clone'
alias gi='git init'
alias gr='git remote add origin'
alias ga='git add .'
alias gcm='git commit -m'
alias gp='git push -u origin master'
alias gf='git push -f origin master'
alias grn='git remote rename origin old-origin'
alias gra='git remote add origin'
alias gpa='git push -u origin --all'
alias gpt='git push -u origin --tags'
alias gpaf="git push -f origin --all"
alias gptf='git push -f origin --tags'
### Changing ls to eza
alias ls='eza -la --color=always --group-directories-first'
alias la='eza -a --color=always --group-directories-first'
alias ll='eza -l --color=always --group-directories-first'
alias lt='eza -aT --color=always --group-directories-first'
### Clear and Neofetch
alias clr='clear'
#alias neofetch='neofetch --off'
alias neo='neofetch'
alias clrn='clear && neofetch'
### Vim to Neovim
alias vim='nvim'
### Other
alias df='df -h'
alias j='jobs'
alias pu='pushd'
alias po='popd'
alias d='dirs -v'
alias h='history'
alias grep='egrep'

PATH=$PATH:/home/greg/.cargo/bin;export PATH;
eval "$(starship init bash)"

neofetch
PATH=$PATH:/home/greg/010editor;export PATH; # ADDED BY INSTALLER - DO NOT EDIT OR DELETE THIS COMMENT - 87FF8EFC-483D-BCAA-D67D-735CF60410D1 905C1B12-23B1-FCCC-B711-75BD7DBBD133

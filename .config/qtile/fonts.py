def groupbox_font():
    return {
        'font': "Hack Nerd Font Mono", 
        'fontsize': 35
        }

def default_font():
    return {
        'font': "Hack Nerd Font Bold",  
        'fontsize': 18
        }

def powerline_font():
    return {
        'font': "Hack Nerd Font Mono", 
        'text': "", 
        'fontsize': 37, 
        'padding': 0
        }

def icon_font():
    return {
        'font': "Hack Nerd Font Mono", 
        'fontsize': 45,
        'padding': 0
        }

def tray_font():
    return {
        'font': "Hack Nerd Font Mono", 
        'fontsize': 45,
        'padding': 20
        }

def temp_icon_font():
    return {
        'font': "Hack Nerd Font Mono", 
        'fontsize': 30,
        'padding': 0
        }

def cpu_icon_font():
    return {
        'font': "Hack Nerd Font Mono", 
        'fontsize': 60,
        'padding': 0
        }

#These are just set to be big.
def os_font():
    return {
        'font': "Hack Nerd Font Mono", 
        'fontsize': 50,
        'padding': 0
        }

def wm_font():
    return {
        'font': "Hack Nerd Font Mono", 
        'fontsize': 55,
        'padding': 4
        }

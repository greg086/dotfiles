import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Key, KeyChord, Screen, Group, Drag, Click, Match
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from typing import List  # noqa: F401
from os import path
import colors
from fonts import groupbox_font, default_font, powerline_font, icon_font, tray_font, temp_icon_font, cpu_icon_font, os_font, wm_font
from theme import init_widgets_screen1, init_widgets_screen2, init_widgets_list, init_screens, init_layout_theme, margin

mod = "mod4"
terminal ="kitty"

keys = [
    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down(),
        desc="Move focus down in stack pane"),
    Key([mod], "j", lazy.layout.up(),
        desc="Move focus up in stack pane"),

    # Adjust Layout
    Key([mod, "control"], "k", lazy.layout.shuffle_down(),
        desc="Move window down in current stack"),
    Key([mod, "control"], "j", lazy.layout.shuffle_up(),
        desc="Move window up in current stack"),
    Key([mod], "l", lazy.layout.grow_main(),
        desc="expand master"),
    Key([mod], "h", lazy.layout.shrink_main(),
        desc="shrink master"),
    Key([mod, "shift"], "l", lazy.layout.grow(),
        desc="expand window"),
    Key([mod, "shift"], "h", lazy.layout.shrink(),
        desc="shrink window"),
    Key([mod, "control"], "f", lazy.layout.flip(),
        desc="flip layout"),

    # Toggle floating 
     Key([mod, "control"], "space", lazy.window.toggle_floating(),
        desc="Toggle Floating"),


    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next(),
        desc="Switch window focus to other pane(s) of stack"),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate(),
        desc="Swap panes of split stack"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    
    Key([mod, "shift"], "t", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),

    #Launch rofi
    Key([mod, "shift"], "Return", lazy.spawn("rofi -i -show drun -modi drun -show-icons"), desc='Drun'),
    Key([mod, "control"], "Return", lazy.spawn("rofi -show window"), desc='Rofi Show Window'),
    
    #Launch dmenu
    Key([mod, "control", "shift"], "Return", lazy.spawn("dmenu_run -p 'Programs ' -x '960' -y '540' -z '1920' -l '27' -bw '8'"), desc='Drun'),

    #Open Log
    Key([mod, "mod1"], "Return", lazy.spawn("alacritty -e nvim /home/greg/.local/share/qtile/qtile.log"), desc="Open Log"),

    #Programs
    Key([mod], "Return", lazy.spawn("kitty -e fish"), desc="Launch terminal"),
    Key([mod], "z", lazy.spawn("brave-bin --password-store=basic"), desc="Launch Brave"),
    Key([mod], "n", lazy.spawn("kitty -e 'ranger'"), desc="Launch Ranger"),
    Key([mod], "p", lazy.spawn("pcmanfm"), desc="Launch PCManFM"),
    Key([mod], "d", lazy.spawn("discord"), desc="Launch Discord"),
    Key([mod], "b", lazy.spawn("blender"), desc="Launch Blender"),
    Key([mod], "g", lazy.spawn("gimp"),    desc="Launch Gimp"),
    Key([mod], "o", lazy.spawn("obs"),    desc="Launch OBS"),
    Key([mod], "m", lazy.spawn("virtualbox"),    desc="Launch Virtual Box"),
    Key([mod], "s", lazy.spawn("steam"),    desc="Launch Steam"),

    #Keychords for Dmenu Scripts
    KeyChord([mod],"x", [
             Key([], "d", lazy.spawn("dotfiles.sh"), desc='dotfiles'),
             Key([], "c", lazy.spawn("colors.sh"), desc='colors'),
             Key([], "x", lazy.spawn("logout_reboot_shutdown.sh"), desc='system'),
             Key([], "s", lazy.spawn("screenshot.sh"), desc='screenshot')
         ]),

    #Keychords for utilities
    KeyChord([mod],"c", [
             Key([], "p", lazy.spawn("pavucontrol"), desc='Audio Config'),
             Key([], "s", lazy.spawn("spectacle"), desc='Screenshots'),
             Key([], "l", lazy.spawn("lxappearance"), desc='Change Theme'),
             Key([], "x", lazy.spawn("xfce4-power-manager-settings"), desc='Power'),
             Key([], "w", lazy.spawn("nitrogen"), desc='Change Wallpaper'),
             Key([], "h", lazy.spawn("kitty -e htop"), desc='System Monitor'),
             Key([], "n", lazy.spawn("kitty -e nvtop"), desc='GPU Monitor'),
             Key([], "g", lazy.spawn("nvidia-settings"), desc='Nvidia Settings') 
         ]),

    Key([mod], "m", lazy.window.toggle_maximize(), desc="Toggle Maximize"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    #Close window
    Key([mod, "shift"], "c", lazy.window.kill(), desc="Kill focused window"),
    #Restart qtile
    Key([mod, "shift"], "r", lazy.restart(), desc="Restart qtile"),
    #Quit qtile
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown qtile"),
    #Spawn command
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
]

#Groups
groups = []
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9",]

#group_labels = ["DEV", "WWW", "SYS", "DOC", "VBOX", "CHAT", "MUS", "VID", "GFX",]
#group_labels = ["1", "2", "3", "4", "5", "6", "7", "8", "9",]
#group_labels = ["󰎦", "󰎩", "󰎬", "󰎮", "󰎰", "󰎵", "󰎸", "󰎻", "󰎾",]
group_labels = ["", "", "󰙯", "", "󰓓", "󰉼", "", "", "󰒋",]


group_layouts = ["monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall"]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))
 
for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=False),
                desc="Move focused window to group {}".format(i.name),
            ),
        ]
    )

layout_theme = init_layout_theme()

layouts = [
    layout.MonadTall(
        max_ratio = 0.95,
        min_ratio = 0.05,
        #single_margin = 0,
        #single_border_width = 0,
        **layout_theme,
        **margin()
    ),
    layout.Floating(
        **layout_theme
    ),
    layout.Max(
        **layout_theme,
        **margin()
    ),
    # layout.Stack(num_stacks=2),
    # Try more layouts by unleashing below layouts.
    # layout.Bsp(**layout_theme),
    # layout.Columns(**layout_theme),
    # layout.Matrix(**layout_theme),
    # layout.MonadWide(**layout_theme),
    # layout.RatioTile(**layout_theme),
    # layout.Tile(**layout_theme),
    # layout.TreeTab(**layout_theme),
    # layout.VerticalTile(**layout_theme)
    # layout.Zoomy(**layout_theme),
]

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())


#Default Widget Settings
#Font applies to the right powerline widgets
widget_defaults = dict(
    font='Hack Nerd Font bold',
    fontsize=15,
    background="#00000000"
)

extension_defaults = widget_defaults.copy()

if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirm'),
    Match(wm_class='dialog'),
    Match(wm_class='download'),
    Match(wm_class='error'),
    Match(wm_class='file_progress'),
    Match(wm_class='notification'),
    Match(wm_class='splash'),
    #Match(wm_class='conky'),
    Match(wm_class='toolbar'),
    Match(wm_class='galculator'),
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='pavucontrol'),  # gitk
    Match(wm_class='xfce4-power-manager-settings'),  # gitk
    Match(wm_class='nitrogen'),  # gitk
    Match(wm_class='lxappearance'),  # gitk
    Match(wm_class='nvidia-settings'),  # gitk
    # Match(wname='branchdialog'),  # gitk
    # Match(wname='pinentry'),  # GPG key password entry
    Match(wm_class='ssh-askpass'),  # ssh-askpass
],
    **layout_theme 
)
auto_fullscreen = True
focus_on_window_activation = "smart"

@hook.subscribe.startup_once
def start_once():
           subprocess.call(['/home/greg/scripts/wm_autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
